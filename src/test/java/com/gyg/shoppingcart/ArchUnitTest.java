package com.gyg.shoppingcart;


import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class ArchUnitTest {
    private JavaClasses jc;

    @BeforeEach
    public void init() {
        this.jc = new ClassFileImporter()
                .importPackages("..com.gyg.shoppingcart");
    }


    //CONTROLLER
    @Test
    public void controllerShouldOnlyDependOnServiceApiAndRecord() {

        classes().that().resideInAnyPackage("..controller..")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..controller..",
                        "..service.api..",
                        "..record..",
                        "java..",
                        "jakarta..",
                        "org.springframework..",
                        "lombok..",
                        "com.fasterxml.jackson..")
                .check(jc);
    }

    //RECORD
    @Test
    public void recordShouldNotDependOnAnyOther() {

        classes().that().resideInAnyPackage("..record")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..record..",
                        "java..",
                        "jakarta..",
                        "com.fasterxml.jackson..",
                        "org.springframework..")
                .check(jc);
    }

    //SERVICE API
    @Test
    public void serviceApiShouldOnlyDependOnRecord() {

        classes().that().resideInAnyPackage("..service.api")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..service.api",
                        "..record..",
                        "java..",
                        "org.springframework..")
                .check(jc);
    }

    //SERVICE
    @Test
    public void serviceShouldOnlyDependOnServiceApiRecordModelAndRepository() {

        classes().that().resideInAnyPackage("..service")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..service..",
                        "..record..",
                        "..model..",
                        "..repository..",
                        "java..",
                        "org.slf4j..",
                        "org.springframework..")
                .check(jc);
    }

    //MODEL
    @Test
    public void modelShouldOnlyDependOnRecord() {

        classes().that().resideInAnyPackage("..model")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..model..",
                        "..record..",
                        "java..",
                        "jakarta..",
                        "lombok..",
                        "org.hibernate..",
                        "org.springframework..")
                .check(jc);
    }

    //REPOSITORY
    @Test
    public void repositoryShouldOnlyDependOnModel() {

        classes().that().resideInAnyPackage("..repository")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..repository",
                        "..model",
                        "java..",
                        "org.springframework..")
                .check(jc);
    }

    //CONFIG
    @Test
    public void configShouldNotDependOnAnyOther() {

        classes().that().resideInAnyPackage("..config")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..config..",
                        "java..",
                        "org.springframework..",
                        "io.swagger..")
                .check(jc);
    }
}
