package com.gyg.shoppingcart.record;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public record MessageDataRecord(
        Long purchaseId,
        String clientId,
        String clientName,
        String clientSurname,
        String email,
        double total,
        double totalWithDiscount,
        List<MovieRecord> moviesList,
        @JsonSerialize(using = LocalDateSerializer.class)
        @JsonDeserialize(using = LocalDateDeserializer.class)
        @JsonFormat(pattern = "dd-MM-yyyy")
        LocalDate date
) implements Serializable {
}
