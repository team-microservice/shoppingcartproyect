package com.gyg.shoppingcart.record;

import java.util.List;

public record CartRecord(
        Long id,
        List<MovieRecord> movies,
        String client,

        double total
) {
    public CartRecord(List<MovieRecord> movies, String client) {
        this(null, movies, client, 0);
    }
}
