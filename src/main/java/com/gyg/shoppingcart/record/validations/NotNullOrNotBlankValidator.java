package com.gyg.shoppingcart.record.validations;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class NotNullOrNotBlankValidator implements ConstraintValidator<NotNullOrNotBlank, String> {
    private String messageIfNull;
    private String messageIfBlank;

    @Override
    public void initialize(NotNullOrNotBlank constraintAnnotation) {
        messageIfNull = constraintAnnotation.messageIfNull();
        messageIfBlank = constraintAnnotation.messageIfBlank();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(messageIfNull).addConstraintViolation();
            return false;
        }

        if (value.trim().isEmpty()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(messageIfBlank).addConstraintViolation();
            return false;
        }

        return true;
    }
}