package com.gyg.shoppingcart.record.validations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = NotNullOrNotBlankValidator.class)
public @interface NotNullOrNotBlank {
    String message() default "El campo no debe estar vacío o ser nulo";

    String messageIfNull() default "El campo no debe ser nulo";

    String messageIfBlank() default "El campo no debe estar vacío";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}