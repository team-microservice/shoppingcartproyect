package com.gyg.shoppingcart.record;

import java.time.LocalDate;

public record DiscountRecord(
        Long id,
        int amount,
        LocalDate startDate,
        LocalDate endDate) {

}
