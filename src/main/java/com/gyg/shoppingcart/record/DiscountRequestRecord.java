package com.gyg.shoppingcart.record;

import com.gyg.shoppingcart.record.validations.NotNullOrNotBlank;
import jakarta.validation.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;

public record DiscountRequestRecord(
        @NotNull(message = "El campo monto no puede ser nulo")
        @Digits(integer = 10, fraction = 0, message = "El campo monto debe ser un número entero.")
        @DecimalMin(value = "0.0", inclusive = false, message = "El campo monto debe ser un valor entero entre 1 y 100")
        @DecimalMax(value = "100.0", inclusive = true, message = "El campo monto debe ser un valor entero entre 1 y 100")
        double amount,

        @NotNullOrNotBlank(message = "La fecha de inicio no puede ser nula o vacía")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        String startDate,
        @NotNullOrNotBlank(message = "La fecha de expiración no puede ser nula o vacía")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        String endDate
) {
}
