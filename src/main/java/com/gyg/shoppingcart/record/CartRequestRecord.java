package com.gyg.shoppingcart.record;

public record CartRequestRecord(
        MovieRecord movies
) {
}
