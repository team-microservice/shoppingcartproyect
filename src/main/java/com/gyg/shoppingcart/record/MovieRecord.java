package com.gyg.shoppingcart.record;

import com.gyg.shoppingcart.record.validations.NotNullOrNotBlank;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;

public record MovieRecord(
        @NotNull(message = "El campo Id no puede ser nulo")
        Long id,
        @NotNullOrNotBlank(messageIfNull = "El campo titulo no debe ser nulo", messageIfBlank = "El campo titulo no debe estar vacío")
        String title,
        @NotNullOrNotBlank(messageIfNull = "El campo imagen no debe ser nulo", messageIfBlank = "El campo imagen no debe estar vacío")
        String image,
        @NotNull(message = "El campo precio no puede ser nulo")
        @DecimalMin(value = "0.0", inclusive = false, message = "El campo precio debe ser un valor mayor a 0")
        double price) {
}
