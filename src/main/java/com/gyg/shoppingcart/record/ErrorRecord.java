package com.gyg.shoppingcart.record;

import org.springframework.http.HttpStatus;

public record ErrorRecord(HttpStatus httpStatus, String message) {

}
