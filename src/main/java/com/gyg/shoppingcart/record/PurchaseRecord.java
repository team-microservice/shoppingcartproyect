package com.gyg.shoppingcart.record;

import com.gyg.shoppingcart.record.validations.NotNullOrNotBlank;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;


import java.time.LocalDate;
import java.util.List;

public record PurchaseRecord(
        Long id,
        @NotNullOrNotBlank(messageIfNull = "La lista de peliculas no debe ser nulo", messageIfBlank = "La lista de peliculas no debe estar vacío")
        List<MovieRecord> moviesList,
        @NotNullOrNotBlank(messageIfNull = "El campo cliente no debe ser nulo", messageIfBlank = "El campo cliente no debe estar vacío")
        String client,
        @NotNull(message = "El campo total no puede ser nulo")
        @DecimalMin(value = "0.0", inclusive = false, message = "El campo total debe ser un valor mayor a 0")
        double total,
        @NotNull(message = "El campo total con descuento no puede ser nulo")
        @DecimalMin(value = "0.0", message = "El campo total con descuento debe ser un valor positivo")
        double totalWithDiscount,
        @NotNullOrNotBlank(messageIfNull = "El campo fecha no debe ser nulo", messageIfBlank = "El campo fecha no debe estar vacío")
        LocalDate date) {
}
