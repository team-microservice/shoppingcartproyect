package com.gyg.shoppingcart.repository;

import com.gyg.shoppingcart.model.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Long> {

    @Query("SELECT d FROM Discount d WHERE d.startDate<=:currentDate AND d.endDate>=:currentDate")
    Discount findActive(@Param("currentDate") LocalDate currenDate);


    @Query("SELECT CASE WHEN COUNT(d) > 0 THEN true ELSE false END FROM Discount d " +
            "WHERE (d.startDate<=:starDate AND d.endDate>=:starDate) OR" +
            " (d.startDate<=:endDate AND d.endDate>=:endDate) OR " +
            "(d.startDate>=:starDate AND d.endDate<=:endDate )")
    boolean existsDicountInDate(@Param("starDate") LocalDate starDate, @Param("endDate") LocalDate endDate);
}
