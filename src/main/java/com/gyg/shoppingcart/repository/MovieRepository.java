package com.gyg.shoppingcart.repository;

import com.gyg.shoppingcart.model.Discount;
import com.gyg.shoppingcart.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {

}
