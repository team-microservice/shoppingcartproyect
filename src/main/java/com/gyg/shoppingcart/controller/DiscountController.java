package com.gyg.shoppingcart.controller;

import com.gyg.shoppingcart.record.DiscountRecord;
import com.gyg.shoppingcart.record.DiscountRequestRecord;
import com.gyg.shoppingcart.service.api.DiscountServiceAPI;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shopping-carts/discounts")
@AllArgsConstructor
public class DiscountController {

    private final DiscountServiceAPI discountServiceAPI;

    @PostMapping()
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> addDiscount(@Valid @RequestBody DiscountRequestRecord discountRequestRecord) {
        DiscountRecord discount = discountServiceAPI.save(discountRequestRecord);
        Map<String, Object> map = new HashMap<>();
        map.put("discount", discount);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PutMapping("/{idDiscount}")
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> updateDiscount(@PathVariable Long idDiscount, @Valid @RequestBody DiscountRequestRecord discountRequestRecord) {
        DiscountRecord discount = discountServiceAPI.update(idDiscount, discountRequestRecord);
        Map<String, Object> map = new HashMap<>();
        map.put("discount", discount);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @DeleteMapping("/{idDiscount}")
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> deleteDiscount(@PathVariable Long idDiscount) {
        discountServiceAPI.delete(idDiscount);
        Map<String, Object> map = new HashMap<>();
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping()
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> listAllDiscounts() {
        List<DiscountRecord> discounts = discountServiceAPI.findAll();
        Map<String, Object> map = new HashMap<>();
        map.put("discounts", discounts);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/{idDiscount}")
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> findDiscountsByID(@PathVariable Long idDiscount) {
        DiscountRecord discounts = discountServiceAPI.find(idDiscount);
        Map<String, Object> map = new HashMap<>();
        map.put("discount", discounts);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/active")
//    @PreAuthorize("hasRole('user-client')")
    @PreAuthorize("hasRole('user-client') OR hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> getActiveDiscount() {
        DiscountRecord discount = discountServiceAPI.findActive();
        Map<String, Object> map = new HashMap<>();
        map.put("discount", discount);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
