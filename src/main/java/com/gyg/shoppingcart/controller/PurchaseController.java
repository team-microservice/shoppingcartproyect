package com.gyg.shoppingcart.controller;

import com.gyg.shoppingcart.record.MessageDataRecord;
import com.gyg.shoppingcart.record.PurchaseRecord;
import com.gyg.shoppingcart.service.CartService;
import com.gyg.shoppingcart.service.api.CartServiceAPI;
import com.gyg.shoppingcart.service.api.PurchaseServiceAPI;
import com.gyg.shoppingcart.service.api.PublisherServiceAPI;
import lombok.AllArgsConstructor;
import org.springframework.amqp.AmqpConnectException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shopping-carts/purchases")
@AllArgsConstructor
@PreAuthorize("hasRole('user-client')")
public class PurchaseController {

    private final PurchaseServiceAPI purchaseServiceAPI;
    private final PublisherServiceAPI publisherServiceAPI;
    private final CartServiceAPI cartServiceAPI;

    @PostMapping("/{idCart}")
    public ResponseEntity<Map<String, Object>> buy(@AuthenticationPrincipal Jwt jwt, @PathVariable Long idCart) {
        String clientId = jwt.getClaim("sub").toString();
        String email = jwt.getClaim("email").toString();
        String clientName = jwt.getClaim("given_name").toString();
        String clientSurname = jwt.getClaim("family_name").toString();
        PurchaseRecord purchase = purchaseServiceAPI.save(idCart);
        Map<String, Object> map = new HashMap<>();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            publisherServiceAPI.sendJsonMessage(
                    new MessageDataRecord(purchase.id(), clientId, clientName, clientSurname,
                            email, purchase.total(), purchase.totalWithDiscount(), purchase.moviesList(), purchase.date()
                    ));
            cartServiceAPI.clearCart(idCart);
            map.put("httpStatus", httpStatus);
            map.put("discount", purchase);
            map.put("message", "La operacion se realizo con exito");
        } catch (AmqpConnectException e) {
            purchaseServiceAPI.deleteById(purchase.id());
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            map.clear();
            Map<String, Object> errors = new HashMap<>();
            errors.put("purchase", "Error al realizar la compra");
            map.put("httpStatus", httpStatus);
            map.put("errors", errors);
        }
        return new ResponseEntity<>(map, httpStatus);
    }

    @GetMapping("/{idClient}")
    public ResponseEntity<Map<String, Object>> findAllByCliente(@PathVariable String idClient) {
        List<PurchaseRecord> purchasesList = purchaseServiceAPI.findAllByClient(idClient);
        Map<String, Object> map = new HashMap<>();
        map.put("purchases", purchasesList);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }


}
