package com.gyg.shoppingcart.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.gyg.shoppingcart.record.ErrorRecord;
import com.gyg.shoppingcart.service.api.exception.AlReadyExistExeption;
import com.gyg.shoppingcart.service.api.exception.BadRequestException;
import com.gyg.shoppingcart.service.api.exception.NotExistException;
import jakarta.persistence.RollbackException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorRecord> all(Exception ex) {
        ErrorRecord errorRecord = new ErrorRecord(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        ex.printStackTrace();
        return new ResponseEntity<>(errorRecord, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<Map<String, Object>> saveException(BadRequestException ex) {
        Map<String, Object> mapResponseEntity = new HashMap<>();
        Map<String, Object> mapErrors = new HashMap<>();
        mapErrors.put(ex.getField(), ex.getMessage());
        mapResponseEntity.put("errors", mapErrors);
        mapResponseEntity.put("httpStatus", ex.getHttpStatus());
        return new ResponseEntity<>(mapResponseEntity, ex.getHttpStatus());
    }

    @ExceptionHandler(value = NotExistException.class)
    public ResponseEntity<Map<String, Object>> notExist(NotExistException ex) {
        Map<String, Object> mapResponseEntity = new HashMap<>();
        Map<String, Object> mapErrors = new HashMap<>();
        mapErrors.put(ex.getField(), ex.getMessage());
        mapResponseEntity.put("errors", mapErrors);
        mapResponseEntity.put("httpStatus", ex.getHttpStatus());
        return new ResponseEntity<>(mapResponseEntity, ex.getHttpStatus());
    }

    @ExceptionHandler(value = AlReadyExistExeption.class)
    public ResponseEntity<Map<String, Object>> alreadyExist(AlReadyExistExeption ex) {
        Map<String, Object> mapResponseEntity = new HashMap<>();
        Map<String, Object> mapErrors = new HashMap<>();
        mapErrors.put(ex.getField(), ex.getMessage());
        mapResponseEntity.put("errors", mapErrors);
        mapResponseEntity.put("httpStatus", ex.getHttpStatus());
        return new ResponseEntity<>(mapResponseEntity, ex.getHttpStatus());
    }


    //Esta validacion es para cuando se matandan datos incorrectos en el @RequetBody que no cumplen con las anotaciones del modelo al que se quiere deserializar
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> validateArgumentExeption(MethodArgumentNotValidException ex) {
        Map<String, ArrayList<String>> errorMap = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(fieldError -> {
            if (errorMap.containsKey(fieldError.getField())) {
                errorMap.get(fieldError.getField()).add(fieldError.getDefaultMessage());
            } else {
                errorMap.put(fieldError.getField(), new ArrayList<String>() {{
                    add(fieldError.getDefaultMessage());
                }});
                //el add esta entre {{}} porque es una clase anonima
            }
        });
        Map<String, Object> requestErrors = new HashMap<>();
        requestErrors.put("errors", errorMap);
        requestErrors.put("httpStatus", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(requestErrors, HttpStatus.BAD_REQUEST);
    }

    //valida las excepciones que se crean cuando no se cumple con las anotaciones del modelo
    //esta validacion ocurre si no se valido previamente en el controller con el @Valid en el @RequestBody
    @ExceptionHandler(value = TransactionSystemException.class)
    public ResponseEntity<Map<String, Object>> validateRollBackExeption(TransactionSystemException ex) {
        Map<String, ArrayList<String>> errorMap = new HashMap<>();
        if (ex.getCause() instanceof RollbackException) {
            if (ex.getCause().getCause() instanceof ConstraintViolationException) {
                ((ConstraintViolationException) ex.getCause().getCause()).getConstraintViolations().forEach(fieldError -> {
                    if (errorMap.containsKey(fieldError.getPropertyPath())) {
                        errorMap.get(fieldError.getPropertyPath()).add(fieldError.getMessageTemplate());
                    } else {
                        errorMap.put(fieldError.getPropertyPath().toString(), new ArrayList<String>() {{
                            add(fieldError.getMessageTemplate());
                        }});
                        //el add esta entre {{}} porque es una clase anonima
                    }
                });
            }
        }


        Map<String, Object> requestErrors = new HashMap<>();
        requestErrors.put("errors", errorMap);
        requestErrors.put("httpStatus", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(requestErrors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<Map<String, Object>> validateFormatException(HttpMessageNotReadableException ex) {
        Map<String, Object> errorMap = new HashMap<>();

        if (ex.getCause() instanceof InvalidFormatException) {
            InvalidFormatException e = ((InvalidFormatException) ex.getCause());
            errorMap.put(e.getPath().get(0).getFieldName(), "El valor " + e.getValue() + " esta en el formato incorrecto");
        }
        errorMap.put("httpStatus", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
    }


}
