package com.gyg.shoppingcart.controller;

import com.gyg.shoppingcart.record.CartRecord;
import com.gyg.shoppingcart.record.MovieRecord;
import com.gyg.shoppingcart.service.api.CartServiceAPI;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/shopping-carts/cart")
@AllArgsConstructor

public class CartController {

    private final CartServiceAPI cartServiceAPI;

    @PostMapping("/{idClient}")
    @PreAuthorize("hasRole('admin-client')")
    public ResponseEntity<Map<String, Object>> saveCart(@PathVariable String idClient) {
        CartRecord cartRecord = cartServiceAPI.saveCart(idClient);
        Map<String, Object> map = new HashMap<>();
        map.put("cart", cartRecord);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/{idClient}")
    @PreAuthorize("hasRole('user-client')")
    public ResponseEntity<Map<String, Object>> findCart(@PathVariable String idClient) {
        CartRecord cart = cartServiceAPI.findCart(idClient);
        Map<String, Object> map = new HashMap<>();
        map.put("cart", cart);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PutMapping("add-movie/{idClient}")
    @PreAuthorize("hasRole('user-client')")
    public ResponseEntity<Map<String, Object>> addMovieToCart(@PathVariable String idClient, @Valid @RequestBody MovieRecord movieRecord) {

        CartRecord cart = cartServiceAPI.addMovieToCart(movieRecord, idClient);
        Map<String, Object> map = new HashMap<>();
        map.put("cart", cart);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }


    @PutMapping("delete-movie/{idClient}/{idMovie}")
    @PreAuthorize("hasRole('user-client')")
//delete-movie/{idClient}/movie/{idMovie} // delete/{idClient}/movie/{idMovie}
    public ResponseEntity<Map<String, Object>> removeMovieFromCart(@PathVariable String idClient, @PathVariable Long idMovie) {
        CartRecord cart = cartServiceAPI.removeMovieFromCart(idClient, idMovie);
        Map<String, Object> map = new HashMap<>();
        map.put("cart", cart);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("total-price/{idCart}")
    @PreAuthorize("hasRole('user-client')")
    public ResponseEntity<Map<String, Object>> getPriceWithDiscount(@PathVariable Long idCart) {
        double price = cartServiceAPI.getPriceWithDiscount(idCart);
        Map<String, Object> map = new HashMap<>();
        map.put("totalPrice", price);
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");

        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
