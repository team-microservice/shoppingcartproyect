package com.gyg.shoppingcart.model;

import com.gyg.shoppingcart.record.PurchaseRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "purchases")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToMany
    @JoinColumn(name = "movies")
    @NotNull(message = "La lista de peliculas no puede ser nula")
    private List<Movie> moviesList; //listado de peliculas agregadas al carrito
    //List para que se pueda repetir la pelicula (Ej: comprar shrek x3 )

    private String client; //nombre del cliente, username o id alfanumerico
    private double total; // sin descuentos aplicados
    private double totalWithDiscount;

    private LocalDate date;

    public Purchase(List<Movie> moviesList, String client, double total, double totalWithDiscount, LocalDate date) {
        this.moviesList = moviesList;
        this.client = client;
        this.total = total;
        this.totalWithDiscount = totalWithDiscount;
        this.date = date;
    }

    public PurchaseRecord toRecord() {
        return new PurchaseRecord(id, moviesList.stream().map(Movie::toRecord).collect(Collectors.toList()),
                client, total, totalWithDiscount, date);
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", moviesList=" + moviesList +
                ", client='" + client + '\'' +
                ", total=" + total +
                ", totalWithDiscount=" + totalWithDiscount +
                ", date=" + date +
                '}';
    }
}
