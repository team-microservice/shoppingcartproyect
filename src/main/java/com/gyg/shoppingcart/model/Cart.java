package com.gyg.shoppingcart.model;

import com.gyg.shoppingcart.record.CartRecord;
import com.gyg.shoppingcart.record.MovieRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "cart")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Cart {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToMany
    @JoinColumn(name = "movies")
    @NotNull(message = "La lista de peliculas no puede ser nula")
    private List<Movie> moviesList; //listado de peliculas agregadas al carrito
    //List para que se pueda repetir la pelicula (Ej: comprar shrek x3 )
    @Column(name = "client", unique = true)
    @NotEmpty(message = "El cliente no puede ser vacío o nulo")
    private String client; //nombre del cliente, username o id alfanumerico
    @NotNull(message = "El campo total no puede ser nulo")
    private double total; // sin descuentos aplicados

    public Cart(Long id, List<Movie> moviesList, String client) {
        this.id = id;
        this.moviesList = moviesList;
        this.client = client;
    }

    public Cart(String client) {
        this.client = client;
        this.moviesList= new ArrayList<>();

    }

    public double calculateTotalWithDiscount(Discount discount) {
        double amount = moviesList.stream().mapToDouble(movie -> movie.getPrice()).sum();
        if (discount != null)
            amount -= (amount * discount.getAmount());
        return amount;
    }

    private void calculateTotal() {
        this.total = moviesList.stream().mapToDouble(movie -> movie.getPrice()).sum();


    }

    public void setMovie(Movie movie) {
        moviesList.add(movie);
        calculateTotal();
    }

    public void removeMovie(Movie movie) {
        moviesList.remove(movie);
        calculateTotal();
    }

    public Purchase buy(Discount discount) {
        double totalWhithDiscount = getTotalPrice(discount);
        List<Movie> moviesForPurchase = new ArrayList<>(moviesList); // Crea una nueva lista con las mismas películas
        return
                new Purchase(
                        moviesForPurchase,
                        this.client,
                        this.total,
                        totalWhithDiscount,
                        LocalDate.now()
                );
    }

    public double getTotalPrice(Discount discount) {
        double totalWhithDiscount = this.total;
        if (discount != null) {
            totalWhithDiscount = discount.calulateDiscount(this.total);
        }
        return totalWhithDiscount;
    }

    public void clean(){
        this.moviesList=new ArrayList<>();
        this.total=0;
    }

    public CartRecord toRecord() {
        List<MovieRecord> movieRecords = this.moviesList != null ?
                this.moviesList.stream().map(
                        movie -> new MovieRecord(movie.getId(), movie.getTitle(), movie.getImage(), movie.getPrice())).collect(Collectors.toList()) :
                Collections.emptyList();

        return new CartRecord(this.id, movieRecords, this.client, this.total);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", moviesList=" + moviesList +
                ", client='" + client + '\'' +
                ", total=" + total +
                '}';
    }
}
