package com.gyg.shoppingcart.model;

import com.gyg.shoppingcart.record.MovieRecord;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Entity
@Table(name = "movies")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Movie { //guarda los tres datos minimos necesarios de la pelicula (vertical catalogo)

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private String title;
    private String image;
    private double price;

    public Movie(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public MovieRecord toRecord() {
        return new MovieRecord(this.id, this.title, this.image, this.price);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                '}';
    }
}
