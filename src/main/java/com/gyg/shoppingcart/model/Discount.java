package com.gyg.shoppingcart.model;

import com.gyg.shoppingcart.record.DiscountRecord;
import com.gyg.shoppingcart.record.validations.NotNullOrNotBlank;
import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Table(name = "discounts")
@NoArgsConstructor

@Getter
@Setter
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    @NotNull(message = "El campo monto no puede ser nulo")
    @DecimalMin(value = "0.0", inclusive = false, message = "El campo monto debe ser un valor entero entre 1 y 100")
    @DecimalMax(value = "100.0", inclusive = true, message = "El campo monto debe ser un valor entero entre 1 y 100")
    private int amount; //monto del descuento
    @NotNull(message = "La fecha de inicio no puede ser nula ")
    private LocalDate startDate; //inicio del descuento
    @NotNull(message = "La fecha de expiración no puede ser nula ")
    private LocalDate endDate; //fin del descuento

    public Discount(Long id, int amount, LocalDate startDate, LocalDate endDate) {
        if (!validateDate(startDate, endDate)) {// se podria modificar por una exception personalizada pero eso modificaria la dependencia del ControllerAdvice
            throw new RuntimeException("La fecha de fin no debe ser menor a la fecha de inicio");
        }
        this.id = id;
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Discount(int amount, LocalDate startDate, LocalDate endDate) {
        if (!validateDate(startDate, endDate)) {
            throw new RuntimeException("La fecha de fin no debe ser menor a la fecha de inicio");
        }
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    private boolean validateDate(LocalDate startDate, LocalDate endDate) {

        if (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
            return true;
        }
        return false;
    }

    public double calulateDiscount(double total) {
        double discount = (double) amount / 100.0; // Convertir el monto a un valor decimal
        return total - (discount * total);
    }

    public DiscountRecord toRecord() {
        return new DiscountRecord(id, amount, startDate, endDate);
    }

    @Override
    public String toString() {
        return "Discount{" +
                "id=" + id +
                ", amount=" + amount +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
