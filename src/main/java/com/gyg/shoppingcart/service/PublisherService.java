package com.gyg.shoppingcart.service;

import com.gyg.shoppingcart.record.MessageDataRecord;
import com.gyg.shoppingcart.service.api.PublisherServiceAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PublisherService implements PublisherServiceAPI {

    @Value("${rabbitmq.routing.json.key}")
    private String jsonRoutingKey;


    @Autowired
    private DirectExchange direct;//toma el valor del direct creado en rabbitMQConfig
    private static final Logger LOGGER = LoggerFactory.getLogger(PublisherService.class);

    private RabbitTemplate rabbitTemplate;


    public PublisherService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void sendJsonMessage(MessageDataRecord messageData) {
        LOGGER.info(String.format("Enviando mensaje: " + messageData.toString()));
        rabbitTemplate.convertAndSend(direct.getName(), jsonRoutingKey, messageData);

    }

}
