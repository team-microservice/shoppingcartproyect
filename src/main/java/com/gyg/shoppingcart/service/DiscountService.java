package com.gyg.shoppingcart.service;

import com.gyg.shoppingcart.model.Discount;
import com.gyg.shoppingcart.record.DiscountRecord;
import com.gyg.shoppingcart.record.DiscountRequestRecord;
import com.gyg.shoppingcart.repository.DiscountRepository;
import com.gyg.shoppingcart.service.api.DiscountServiceAPI;
import com.gyg.shoppingcart.service.api.exception.AlReadyExistExeption;
import com.gyg.shoppingcart.service.api.exception.BadRequestException;
import com.gyg.shoppingcart.service.api.exception.NotExistException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DiscountService implements DiscountServiceAPI {

    private DiscountRepository discountRepository;

    @Override
    public DiscountRecord save(DiscountRequestRecord discountRequestRecord) {
        Discount discount = validateDiscountRequestRecord(discountRequestRecord);
        if (discountRepository.existsDicountInDate(discount.getStartDate(), discount.getEndDate())) {
            throw new AlReadyExistExeption("Ya existe un descuento activo para esta fecha", "discount");
        }
        discountRepository.save(discount);
        return discount.toRecord();
    }

    @Override
    public DiscountRecord update(Long id, DiscountRequestRecord discountRequestRecord) {
        Discount discount = discountRepository.findById(id).orElseThrow(
                () -> new NotExistException("El descuento con el id: " + id + " no existe", "id"));
        discount = validateDiscountRequestRecord(discountRequestRecord, discount);
        discount.setId(id);
        discountRepository.save(discount);
        return discount.toRecord();

    }

    @Override
    public List<DiscountRecord> findAll() {
        return discountRepository.findAll().stream().map(Discount::toRecord).collect(Collectors.toList());
    }

    @Override
    public DiscountRecord find(Long id) {
        Optional<Discount> discount = discountRepository.findById(id);
        if (!discount.isPresent()) {
            throw new NotExistException("No existe ningun descuento con el id: " + id, "id");
        }
        return discount.get().toRecord();
    }

    @Override
    public DiscountRecord findActive() {
        Optional<Discount> discount = Optional.ofNullable(discountRepository.findActive(LocalDate.now()));
        if (!discount.isPresent()) {
            throw new NotExistException("No existe ningun descuento vigente", "discount");
        }
        return discount.get().toRecord();
    }

    @Override
    public void delete(Long id) {
        Optional<Discount> discount = discountRepository.findById(id);
        if (!discount.isPresent())
            throw new NotExistException("No existe un descuento para el id " + id, "id");

        discountRepository.delete(discount.get());
    }

    private Discount validateDiscountRequestRecord(DiscountRequestRecord discountRequestRecord, Discount discount) {
        LocalDate startDate = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (discountRequestRecord.startDate() == null) {
            throw new BadRequestException("La fecha no debe ser nula", "startDate");
        }
        try {
            startDate = LocalDate.parse(discountRequestRecord.startDate(), formatter);
            if (startDate.isBefore(LocalDate.now()) && !startDate.equals(discount.getStartDate())) {
                throw new BadRequestException("La fecha de inicio no puede ser menor a la fecha actual", "startDate");
            }
        } catch (java.time.format.DateTimeParseException e) {
            throw new BadRequestException("La fecha de inicio no cumple con el formato yyyy-MM-dd", "startDate");
        }

        LocalDate endDate = null;
        if (discountRequestRecord.endDate() == null) {
            throw new BadRequestException("La fecha de expiración no debe ser nula", "endDate");
        }
        try {
            endDate = LocalDate.parse(discountRequestRecord.endDate(), formatter);
            if (endDate.isBefore(LocalDate.now()) && !endDate.equals(discount.getEndDate())) {
                throw new BadRequestException("La fecha de expiración no puede ser menor a la fecha actual", "endDate");
            }
        } catch (java.time.format.DateTimeParseException e) {
            throw new BadRequestException("La fecha de expiración no cumple con el formato yyyy-MM-dd", "endDate");
        }
        if (startDate.isAfter(endDate)) {
            throw new BadRequestException("La fecha de inicio debe ser menor o igual la fecha de fin", "startDate");
        }
        return new Discount((int) discountRequestRecord.amount(), startDate, endDate);

    }

    private Discount validateDiscountRequestRecord(DiscountRequestRecord discountRequestRecord) {
        LocalDate startDate = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if (discountRequestRecord.startDate() == null) {
            throw new BadRequestException("La fecha no debe ser nula", "startDate");
        }
        try {
            startDate = LocalDate.parse(discountRequestRecord.startDate(), formatter);
            if (startDate.isBefore(LocalDate.now())) {
                throw new BadRequestException("La fecha de inicio no puede ser menor a la fecha actual", "startDate");
            }
        } catch (java.time.format.DateTimeParseException e) {
            throw new BadRequestException("La fecha de inicio no cumple con el formato yyyy-MM-dd", "startDate");
        }

        LocalDate endDate = null;
        if (discountRequestRecord.endDate() == null) {
            throw new BadRequestException("La fecha de expiración no debe ser nula", "endDate");
        }
        try {
            endDate = LocalDate.parse(discountRequestRecord.endDate(), formatter);
            if (endDate.isBefore(LocalDate.now())) {
                throw new BadRequestException("La fecha de expiración no puede ser menor a la fecha actual", "endDate");
            }
        } catch (java.time.format.DateTimeParseException e) {
            throw new BadRequestException("La fecha de expiración no cumple con el formato yyyy-MM-dd", "endDate");
        }
        if (startDate.isAfter(endDate)) {
            throw new BadRequestException("La fecha de inicio debe ser menor o igual la fecha de expiración", "startDate");
        }
        return new Discount((int) discountRequestRecord.amount(), startDate, endDate);

    }
}
