package com.gyg.shoppingcart.service.api;

import com.gyg.shoppingcart.record.PurchaseRecord;

import java.util.List;

public interface PurchaseServiceAPI {

    PurchaseRecord save(Long idCart);

    List<PurchaseRecord> findAllByClient(String idClient);

    void deleteById(Long id);


}
