package com.gyg.shoppingcart.service.api;


import com.gyg.shoppingcart.model.Cart;
import com.gyg.shoppingcart.record.CartRecord;
import com.gyg.shoppingcart.record.MovieRecord;
import com.gyg.shoppingcart.service.api.exception.NotExistException;

public interface CartServiceAPI {
    CartRecord saveCart(String idClient);

    CartRecord findCart(String client);

    CartRecord addMovieToCart(MovieRecord movieRecord, String client);

    CartRecord removeMovieFromCart(String idClient, Long idMovie);

    double getPriceWithDiscount(Long idCart);

    void clearCart(Long id);


}
