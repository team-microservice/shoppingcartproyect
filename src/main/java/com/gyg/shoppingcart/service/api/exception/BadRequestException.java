package com.gyg.shoppingcart.service.api.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BadRequestException extends RuntimeException {
    HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
    String field;

    public BadRequestException(String message, String field) {
        super(message);
        this.field = field;

    }
}
