package com.gyg.shoppingcart.service.api;

import com.gyg.shoppingcart.record.DiscountRecord;
import com.gyg.shoppingcart.record.DiscountRequestRecord;

import java.util.List;

public interface DiscountServiceAPI {
    DiscountRecord save(DiscountRequestRecord discountRequestRecord);

    DiscountRecord update(Long id, DiscountRequestRecord discountRequestRecord);

    List<DiscountRecord> findAll();

    DiscountRecord find(Long id);

    DiscountRecord findActive();

    void delete(Long id);
}
