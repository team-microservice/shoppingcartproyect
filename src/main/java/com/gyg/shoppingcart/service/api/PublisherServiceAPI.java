package com.gyg.shoppingcart.service.api;

import com.gyg.shoppingcart.record.MessageDataRecord;

public interface PublisherServiceAPI {

    void sendJsonMessage(MessageDataRecord messageDataRecord);
}
