package com.gyg.shoppingcart.service.api.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class AlReadyExistExeption extends RuntimeException {
    HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
    String field;

    public AlReadyExistExeption(String message, String field) {
        super(message);
        this.field = field;
    }
}
