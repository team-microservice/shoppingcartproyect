package com.gyg.shoppingcart.service.api.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class NotExistException extends RuntimeException {
    String field;
    HttpStatus httpStatus = HttpStatus.NOT_FOUND;


    public NotExistException(String message, String field) {
        super(message);
        this.field = field;
    }
}
