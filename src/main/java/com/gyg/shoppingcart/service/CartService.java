package com.gyg.shoppingcart.service;

import com.gyg.shoppingcart.model.Cart;
import com.gyg.shoppingcart.model.Discount;
import com.gyg.shoppingcart.model.Movie;
import com.gyg.shoppingcart.record.CartRecord;
import com.gyg.shoppingcart.record.MovieRecord;
import com.gyg.shoppingcart.repository.CartRepository;
import com.gyg.shoppingcart.repository.DiscountRepository;
import com.gyg.shoppingcart.repository.MovieRepository;
import com.gyg.shoppingcart.service.api.CartServiceAPI;
import com.gyg.shoppingcart.service.api.exception.AlReadyExistExeption;
import com.gyg.shoppingcart.service.api.exception.NotExistException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CartService implements CartServiceAPI {

    private CartRepository cartRepository;
    private MovieRepository movieRepository;
    private DiscountRepository discountRepository;

    @Override
    public CartRecord saveCart(String client) {
        if (cartRepository.findByClient(client).isPresent()) {
            throw new AlReadyExistExeption("Ya existe un carrito para el cliente " + client, "client");
        }
        Cart cart = new Cart(client);
        cartRepository.save(cart);
        return cart.toRecord();

    }

    @Override
    public CartRecord findCart(String client) {
        Optional<Cart> cart = cartRepository.findByClient(client);

        if (!cart.isPresent()) {
            saveCart(client);
            cart = cartRepository.findByClient(client);
        }

        return cart.get().toRecord();
    }

    @Override
    public CartRecord addMovieToCart(MovieRecord movieRecord, String client) {
        //HAY QUE VALIDAR QUE EL CLIENTE EXISTA
        Optional<Cart> cartOpt = cartRepository.findByClient(client);

        if (!cartOpt.isPresent()) {
            saveCart(client);
            cartOpt = cartRepository.findByClient(client);
        }

        Cart cart = cartOpt.get();
        Movie movie = new Movie(movieRecord.id(), movieRecord.title(), movieRecord.image(), movieRecord.price());
        movieRepository.save(movie);
        cart.setMovie(movie);
        cartRepository.save(cart);
        return cart.toRecord();
    }

    @Override
    public CartRecord removeMovieFromCart(String idClient, Long idMovie) {
        Cart cart = cartRepository.findByClient(idClient).orElseThrow(() -> new NotExistException("El carrito no existe para el cliente de id " + idClient, "cart"));
        boolean exist = false;
        for (Movie movie : cart.getMoviesList()) {
            if (movie.getId().equals(idMovie)) {
                exist = true;
                cart.removeMovie(movie);
                break;
            }
        }
        if (!exist)
            throw new NotExistException("La pelicula con el id " + idMovie + " no existe en el carrito", "movieId");

        cart = cartRepository.save(cart);
        return cart.toRecord();
    }

    @Override
    public double getPriceWithDiscount(Long idCart) {
        Cart cart = cartRepository.findById(idCart).orElseThrow(() -> new NotExistException("No existe un carrito con el id: " + idCart, "cart"));
        LocalDate currentDate = LocalDate.now();
        Discount discount = discountRepository.findActive(currentDate);
        double price = cart.getTotalPrice(discount);
        return price;
    }

    @Override
    public void clearCart(Long id) {
        Cart cart = cartRepository.findById(id).orElseThrow(() -> new NotExistException("No existe un carrito con el id: " + id, "id"));
        cart.clean();
        cartRepository.save(cart);
    }

}
