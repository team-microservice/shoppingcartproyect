package com.gyg.shoppingcart.service;

import com.gyg.shoppingcart.model.Cart;
import com.gyg.shoppingcart.model.Discount;
import com.gyg.shoppingcart.model.Purchase;
import com.gyg.shoppingcart.record.PurchaseRecord;
import com.gyg.shoppingcart.repository.CartRepository;
import com.gyg.shoppingcart.repository.DiscountRepository;
import com.gyg.shoppingcart.repository.PurchaseRepository;
import com.gyg.shoppingcart.service.api.PurchaseServiceAPI;
import com.gyg.shoppingcart.service.api.exception.NotExistException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PurchaseService implements PurchaseServiceAPI {

    private PurchaseRepository purchaseRepository;
    private DiscountRepository discountRepository;
    private CartRepository cartRepository;

    @Override
    public PurchaseRecord save(Long idCart) {
        Cart cart = cartRepository.findById(idCart).orElseThrow(() -> new NotExistException("No existe un carrito con el id: " + idCart, "id"));
        LocalDate currentDate = LocalDate.now();
        Discount discount = discountRepository.findActive(currentDate);
        Purchase purchase = cart.buy(discount);
        purchaseRepository.save(purchase);
        return purchase.toRecord();
    }


    @Override
    public List<PurchaseRecord> findAllByClient(String idClient) {
        return purchaseRepository.findAllByClient(idClient).stream().map(Purchase::toRecord).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        purchaseRepository.deleteById(id);
    }
//    Purchase validatePurchase(PurchaseRequestRecord purchaseRequestRecord) {
//        List<Movie> movieList = purchaseRequestRecord.moviesList().stream().
//                map(id -> movieRepository.findById(id)
//                        .orElseThrow(() -> new NotExistException("No existe una pelicula con el id: " + id)))
//                .collect(Collectors.toList());
//
//        LocalDate date;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        try {
//            date = LocalDate.parse(purchaseRequestRecord.date(), formatter);
//        } catch (java.time.format.DateTimeParseException e) {
//            throw new BadRequestException("La fecha no cumple con el formato yyyy-MM-dd");
//        }
//        double totalWhithDiscount = 0;
//        Discount discount = discountRepository.findActive(LocalDate.now());
//        if (discount != null) {
//            totalWhithDiscount = discount.calulateDiscount(purchaseRequestRecord.total());
//        }
//
//        return (new Purchase(movieList,
//                purchaseRequestRecord.client(),
//                purchaseRequestRecord.total(),
//                totalWhithDiscount,
//                date));
//    }
}
