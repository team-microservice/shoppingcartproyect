package com.gyg.shoppingcart;

import com.gyg.shoppingcart.repository.DiscountRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;


@SpringBootApplication
public class ShoppingCartProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartProjectApplication.class, args);
        //esto para cuando queremos probar los repository con el debug
//        ApplicationContext context = SpringApplication.run(ShoppingCartProjectApplication.class, args);
//        DiscountRepository discountRepository = context.getBean(DiscountRepository.class);
//        discountRepository.existsDicountInDate(LocalDate.of(2023, 10, 03), LocalDate.of(2023, 12, 12));
//        System.out.println();

    }


    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("http://localhost:5174").allowedOrigins("*").allowedMethods("*").allowedHeaders("*");
            }
        };
    }
}

