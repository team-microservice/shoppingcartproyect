package com.gyg.shoppingcart.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.queue.json.name.mongodb}")
    private String jsonQueueMongoDB;
    @Value("${rabbitmq.queue.json.name.mail}")
    private String jsonQueueMail;

    @Value("${rabbitmq.routing.json.key}")
    private String jsonRoutingKey;

    @Bean
    public Queue jsonQueueMongoDB() {
        return new Queue(jsonQueueMongoDB);
    }

    @Bean
    public Queue jsonQueueMail() {
        return new Queue(jsonQueueMail);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    public Binding jsonBindingMongoDB() {
        return BindingBuilder.bind(jsonQueueMongoDB()).to(exchange()).with(jsonRoutingKey);
    }

    @Bean
    public Binding jsonBindingMail() {
        return BindingBuilder.bind(jsonQueueMail()).to(exchange()).with(jsonRoutingKey);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }


}
